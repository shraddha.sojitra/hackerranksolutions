import UIKit

var str = "Hello, playground"

func minimumBribes(q: [Int]) -> Void {
       var swap = 0
       var bribes: Int
       var pos = 0
       
       for obj in 0..<q.count {
           var j = 0
           bribes = q[pos] - (pos+1)
           if bribes > 2 {
               print("Too chaotic")
               return
           }
           if q[obj] - 2 > 0 {
               j = q[obj] - 2
           }
           
           while j <= obj {
               if q[j] > q[obj] {
                   swap = swap + 1
               }
               j = j + 1
           }
           pos = pos + 1
       }
       print(swap)
   }
