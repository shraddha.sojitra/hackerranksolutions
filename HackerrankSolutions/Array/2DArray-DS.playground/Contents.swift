import UIKit


func rotLeft(a: [Int], d: Int) -> [Int] {

 var newArray = [Int]()
    var newOffSet = d
    if (newOffSet > a.count) {
        newOffSet = d % a.count
    }
    
    for index in newOffSet...a.count-1 {
          newArray.append(a[index])
    }
    
    for index in 0...d-1 {
        newArray.append(a[index])
    }
    return newArray
}
