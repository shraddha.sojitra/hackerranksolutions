import UIKit



func minimumSwaps(arr: [Int]) -> Int {

       var swap = 0
       var array = arr
       for i in 0..<array.count {
           while (i+1 != array[i]) {
               let temp = array[array[i]-1]
               array[array[i]-1] = array[i]
               array[i] = temp
               swap += 1
           }
       }
       return swap
   }

print(minimumSwaps(arr: [2, 3, 4, 1, 5]))
