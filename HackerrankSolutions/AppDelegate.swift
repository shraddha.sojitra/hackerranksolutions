//
//  AppDelegate.swift
//  HackerrankSolutions
//
//  Created by Shraddha Sojitra on 20/12/19.
//  Copyright © 2019 Shraddha Sojitra. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
        
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        print(repeatedString(s: "a", n: 1000000000000))
        return true
    }
    
    func repeatedString(s: String, n: Int) -> Int {

        var infiniteStr: String = s
        
        var count: Int = 0

        if s.count == 1 {
            return n
        }
        
        while infiniteStr.count < n {
            infiniteStr.append(s)
            
            if infiniteStr.count > n {
                infiniteStr.removeLast(infiniteStr.count - n)
            }
        }
                
        
        print(infiniteStr)
        
        for char in infiniteStr {
            if char == "a" {
                count += 1
            }
        }
        
        return count
    }
}

