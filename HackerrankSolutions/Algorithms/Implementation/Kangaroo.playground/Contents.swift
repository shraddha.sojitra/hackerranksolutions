import Foundation

// Complete the kangaroo function below.

private func kangarooRaceSimulator(first: inout (position: Int, speed: Int), second: inout (position: Int, speed: Int)) -> String {
    var kangaroosWillMeet: Bool = false

    var kangarooIsAbleToCatchUp: Bool {
        if first.position > second.position && first.speed >= second.speed {
            return false
        } else if first.position < second.position && first.speed <= second.speed {
            return false
        } else { return true }
    }

    while kangarooIsAbleToCatchUp {
        kangaroosWillMeet = first.position == second.position ? true : false
        first.position = first.position + first.speed
        second.position = second.position + second.speed
    }

    return kangaroosWillMeet ? "YES" : "NO"
}


let stdout = ProcessInfo.processInfo.environment["OUTPUT_PATH"]!
FileManager.default.createFile(atPath: stdout, contents: nil, attributes: nil)
let fileHandle = FileHandle(forWritingAtPath: stdout)!

guard let x1V1X2V2Temp = readLine() else { fatalError("Bad input") }
let x1V1X2V2 = x1V1X2V2Temp.split(separator: " ").map{ String($0) }

guard let x1 = Int(x1V1X2V2[0].trimmingCharacters(in: .whitespacesAndNewlines))
else { fatalError("Bad input") }

guard let v1 = Int(x1V1X2V2[1].trimmingCharacters(in: .whitespacesAndNewlines))
else { fatalError("Bad input") }

guard let x2 = Int(x1V1X2V2[2].trimmingCharacters(in: .whitespacesAndNewlines))
else { fatalError("Bad input") }

guard let v2 = Int(x1V1X2V2[3].trimmingCharacters(in: .whitespacesAndNewlines))
else { fatalError("Bad input") }

var firstKangaroo: (position: Int, speed: Int) = (x1, v1)
var secondKangaroo: (position: Int, speed: Int) = (x2, v2)

let result = kangarooRaceSimulator(first: &firstKangaroo, second: &secondKangaroo)

fileHandle.write(result.data(using: .utf8)!)
fileHandle.write("\n".data(using: .utf8)!)
