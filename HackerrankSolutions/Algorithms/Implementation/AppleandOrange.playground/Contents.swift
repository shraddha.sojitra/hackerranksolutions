import UIKit

var str = "Hello, playground"

func countApplesAndOranges(s: Int, t: Int, a: Int, b: Int, apples: [Int], oranges: [Int]) -> Void {
        let house: (left: Int, right: Int) = (s, t)

       var numberOfApplesThatFellOnHouse: Int = 0
       for apple in apples {
           if fruitFellOnHouse(house, from: a, distance: apple) { numberOfApplesThatFellOnHouse += 1 }
       }
    
    var numberOfOrangesThatFellOnHouse: Int = 0
        for orange in oranges {
            if fruitFellOnHouse(house, from: b, distance: orange) { numberOfOrangesThatFellOnHouse += 1 }
        }
    print(numberOfApplesThatFellOnHouse)
    print(numberOfOrangesThatFellOnHouse)

}

private func fruitFellOnHouse(_ house: (left: Int, right: Int), from tree: Int, distance: Int) -> Bool {
    if (tree + distance) >= house.left && (tree + distance) <= house.right{
        return true
    } else {
        return false
    }
}
