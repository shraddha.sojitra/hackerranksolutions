import UIKit

var str = "Hello, playground"

func gradingStudents(grades: [Int]) -> [Int] {
    var result: [Int] = []
    for grade in grades {
        if grade >= 38, (grade % 5) > 2 {
                result.append(grade + (5 - (grade % 5)))
        } else {
            result.append(grade)
        }
    }
    return result
}
