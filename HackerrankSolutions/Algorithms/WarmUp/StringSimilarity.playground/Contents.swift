import UIKit

var str = "Hello, playground"

func stringSimilarity(s: String) -> Int {
       
       var str = s
       var count = 0
       while !str.isEmpty {
           for (idx,char) in str.enumerated() {
               if char == s[s.index(s.startIndex, offsetBy: idx)] {
                   count += 1
               } else {
                   break
               }
           }
           str.removeFirst()
       }
       return count
   }
