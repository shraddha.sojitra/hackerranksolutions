import UIKit

var str = "Hello, playground"

func lonelyinteger(a: [Int]) -> Int {

        var uniqueInt: Int = 0
        var counts: [Int: Int] = [:]
        for item in a {
            counts[item] = (counts[item] ?? 0) + 1
        }

        for (key, value) in counts {
            if value == 1 {
                uniqueInt = key
            }
        }
        return uniqueInt
}
