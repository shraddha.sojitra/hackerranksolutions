import UIKit

var str = "Hello, playground"

func birthdayCakeCandles(ar: [Int]) -> Int {
    let numbers = ar.compactMap { Int($0) }
    let maxNumber = numbers.max()
    let occurrences = numbers.filter { $0 == maxNumber }
    return occurrences.count
}
