import UIKit

var str = "Hello, playground"

func plusMinus(arr: [Int]) -> Void {
   var positives = 0
    var negatives = 0
    var zeroes = 0

    for i in 0..<arr.count {
        let x = arr[i]
        if (x < 0) {
            negatives += 1
        } else if (x == 0) {
            zeroes += 1
        } else {
            positives += 1
        }
    }
    
    let n2 : Double = Double(arr.count)
    print(Double(positives) / n2)
    print(Double(negatives) / n2)
    print(Double(zeroes) / n2)

}
