import UIKit



//     #
//    ##
//   ###
//  ####
// #####
//######

func staircase(n: Int) {

    var hash = "#"
    for i in 0..<n {
        let spaces = String(repeating: " ", count: n - i - 1)
        print(spaces + hash)
        hash += "#"
    }
}
