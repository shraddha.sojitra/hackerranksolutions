import UIKit

var str = "Hello, playground"


func miniMaxSum(arr: [Int]) -> Void {
    let numbers = arr.compactMap { ($0) }.sorted()
    
   let minSum = numbers.prefix(4).reduce(0, +)
   let maxSum = numbers.suffix(4).reduce(0, +)
   print(minSum, maxSum)
}
