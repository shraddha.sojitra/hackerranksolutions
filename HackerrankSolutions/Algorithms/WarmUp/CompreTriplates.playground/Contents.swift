import UIKit
import Foundation


    
      func compareTriplets(a: [Int], b: [Int]) -> [Int] {
          
          var aScore: Int = 0
          var bScore: Int = 0
          
          for index in 0..<a.count {
              if a[index] > b[index] {
                  aScore += 1
              } else if a[index] < b[index] {
                  bScore += 1
              }
          }
          return [aScore,bScore]
      }
