import UIKit

var str = "Hello, playground"

func lonelyinteger(a: [Int]) -> [Int] {
      return [contiguousTotal(a),nonContiguousTotal(a)]
  }
  
  func nonContiguousTotal(_ array:[Int]) -> Int {
      
      let filteredArray = array.filter { (value) -> Bool in
          return value > 0
      }
      
      return filteredArray.isEmpty ? array.max()! : filteredArray.reduce(0, +)
  }

  func contiguousTotal(_ array:[Int]) -> Int {
      
      var maxEndingHere = array[0]
      var maxSoFar = array[0]
      
      for i in 1..<array.count {
          
          let element = array[i]
          
          maxEndingHere = max(element, maxEndingHere + element)
          maxSoFar = max(maxSoFar, maxEndingHere)
      }
      
      return maxSoFar
  }
