import UIKit

var str = "Hello, playground"

func chocolateFeast(n: Int, c: Int, m: Int) -> Int {
     var ch = n/c
     var all_ch = ch
     while (ch>1) {
         all_ch += ch/m
         ch = ch/m + ch%m
     }
     return all_ch
 }

print(chocolateFeast(n: 10, c: 2, m: 5))
